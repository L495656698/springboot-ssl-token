package com.neusoft.satoken.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Data
@Configuration
@ConfigurationProperties(prefix = "sa-oauth2-app-info")
public class SaOauth2AppInfo  {

    private Integer tokenLength = 16;

    private Integer accessTokenTimeout = 600;

    private Integer refreshTokenTimeout = 3600;

}

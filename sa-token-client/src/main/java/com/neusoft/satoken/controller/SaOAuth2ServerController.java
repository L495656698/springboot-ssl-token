package com.neusoft.satoken.controller;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.context.model.SaRequest;
import cn.dev33.satoken.oauth2.config.SaOAuth2Config;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import com.neusoft.satoken.util.SaOath2UtilImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * 认证相关
 */
@RestController
public class SaOAuth2ServerController {

    @Autowired
    SaOath2UtilImpl saOath2Util;

    /**
     * 获取 access_token、refresh_token
     * @return
     */
    @RequestMapping("/oauth2/token")
    public Object getToken() {
        System.out.println("------- 进入请求: " + SaHolder.getRequest().getUrl());
        try {
            Map<String, Object> accessTokenAndRefreshToken = saOath2Util.createAccessTokenAndRefreshToken();
            return SaResult.data(accessTokenAndRefreshToken);
        } catch (UnsupportedEncodingException e) {
            return SaResult.error(e.getMessage());
        }

    }

    /**
     * 刷新 access_token
     * @return
     */
    @RequestMapping("/oauth2/refresh")
    public Object refreshToken() {
        System.out.println("------- 进入请求: " + SaHolder.getRequest().getUrl());
        SaRequest request = SaHolder.getRequest();
        String accessToken = request.getParam("access_token");
        String reFreshToken = request.getParam("refresh_token");
        saOath2Util.checkRefreshTokenValid(accessToken,reFreshToken);
        return getToken();
    }


    // Sa-OAuth2 定制化配置
    @Autowired
    public void setSaOAuth2Config(SaOAuth2Config cfg) {
        cfg.
                // 配置：未登录时返回的View
                        setNotLoginView(() -> {
                    String msg = "当前会话在SSO-Server端尚未登录，请先访问"
                            + "<a href='/oauth2/doLogin?name=sa&pwd=123456' target='_blank'> doLogin登录 </a>"
                            + "进行登录之后，刷新页面开始授权";
                    return msg;
                }).
                // 配置：登录处理函数
                        setDoLoginHandle((name, pwd) -> {
                    if("zhang".equals(name) && "123456".equals(pwd)) {
                        StpUtil.login(10001);
                        return SaResult.ok();
                    }
                    return SaResult.error("账号名或密码错误");
                }).
                // 配置：确认授权时返回的View
                        setConfirmView((clientId, scope) -> {
                    String msg = "<p>应用 " + clientId + " 请求授权：" + scope + "</p>"
                            + "<p>请确认：<a href='/oauth2/doConfirm?client_id=" + clientId + "&scope=" + scope + "' target='_blank'> 确认授权 </a></p>"
                            + "<p>确认之后刷新页面</p>";
                    return msg;
                })
        ;
    }
}
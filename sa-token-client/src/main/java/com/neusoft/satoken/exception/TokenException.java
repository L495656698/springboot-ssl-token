package com.neusoft.satoken.exception;


import cn.dev33.satoken.util.SaFoxUtil;

public class TokenException extends RuntimeException {
    /**
     * 序列化版本号
     */
    private static final long serialVersionUID = 680612954529013045L;
    
    private static final int ACCESS_TOKEN_EXCEPTION_CODE = 401;

    public static final String ACCESS_TOKEN_TIMEOUT = "access_token 过期";
    public static final String REFRESH_TOKEN_TIMEOUT = "refresh_token 过期";

    /**
     * 异常细分状态码 
     */
    private int code = ACCESS_TOKEN_EXCEPTION_CODE;

    /**
     * 构建一个异常
     *
     * @param code 异常细分状态码 
     */
    public TokenException(int code) {
        super();
        this.code = code;
    }


    /**
     * 构建一个异常
     *
     * @param message 异常描述信息
     */
    public TokenException(String message) {
        super(message);
    }

    /**
     * 构建一个异常
     *
     * @param code 异常细分状态码 
     * @param message 异常信息
     */
    public TokenException(int code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * 构建一个异常
     *
     * @param cause 异常对象
     */
    public TokenException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个异常
     *
     * @param message 异常信息
     * @param cause 异常对象
     */
    public TokenException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 获取异常细分状态码
     * @return 异常细分状态码
     */
    public int getCode() {
        return code;
    }

    /**
     * 写入异常细分状态码 
     * @param code 异常细分状态码
     * @return 对象自身
     */
    public TokenException setCode(int code) {
        this.code = code;
        return this;
    }

    /**
     * 如果flag==true，则抛出message异常 
     * @param flag 标记
     * @param message 异常信息 
     */
    public static void throwBy(boolean flag, String message) {
        if(flag) {
            throw new RuntimeException(message);
        }
    }

    /**
     * 如果value==null或者isEmpty，则抛出message异常 
     * @param value 值 
     * @param message 异常信息 
     */
    public static void throwByNull(Object value, String message) {
        if(SaFoxUtil.isEmpty(value)) {
            throw new RuntimeException(message);
        }
    }   
}

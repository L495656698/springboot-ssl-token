package com.neusoft.satoken.exception;

import cn.dev33.satoken.util.SaResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    // 全局异常拦截
    @ExceptionHandler
    public SaResult handlerException(Exception e) {
        if (e instanceof TokenException){
            return SaResult.get(401,e.getMessage(),null);
        }
        return SaResult.error(e.getMessage());
    }

//    // 全局异常拦截
//    @ExceptionHandler
//    public SaResult handlerException(Exception e) {
//        e.printStackTrace();
//        return SaResult.error(e.getMessage());
//    }
}
package com.neusoft.satoken.util;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class HttpSSLClientUtilTest {

    private final static String TEST_URL = "https://127.0.0.1:8080/oauth2/token";

    @Autowired
    HttpSSLClientUtil httpSSLClientUtil;

    @Test
    public void doGet() {
        String doGet = HttpSSLClientUtil.doGet(TEST_URL);
        System.out.println(doGet);
    }
}
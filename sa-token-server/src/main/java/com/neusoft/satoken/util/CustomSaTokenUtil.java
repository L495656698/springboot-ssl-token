package com.neusoft.satoken.util;

import cn.dev33.satoken.SaManager;
import cn.dev33.satoken.config.SaTokenConfig;
import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.context.model.SaRequest;
import cn.dev33.satoken.util.SaFoxUtil;
import cn.dev33.satoken.util.SaTokenConsts;
import com.neusoft.satoken.config.SaOauth2AppInfo;
import com.neusoft.satoken.exception.TokenException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.Map;


@Component
public class CustomSaTokenUtil  {
    private static final String ACCESS_TOKEN_KEY_PREFIX = "access_token_";
    private static final String REFRESH_TOKEN_KEY_PREFIX = "refresh_token_";
    @Autowired
    SaOauth2AppInfo saOauth2AppInfo;

    @Autowired
    RedisOperator redisOperator;

    /**
     * 生成access_token、refresh_token
     * @throws UnsupportedEncodingException
     */
    public Map<String, Object> createAccessTokenAndRefreshToken() throws UnsupportedEncodingException {
        String accessToken = SaFoxUtil.getRandomString(saOauth2AppInfo.getTokenLength());
        String refreshToken = SaFoxUtil.getRandomString(saOauth2AppInfo.getTokenLength());

        long expiresTime = System.currentTimeMillis() + (saOauth2AppInfo.getAccessTokenTimeout() * 1000);
        long refreshExpiresTime = System.currentTimeMillis() + (saOauth2AppInfo.getRefreshTokenTimeout() * 1000);

        //塞到redis中
        String accessTokenKey = ACCESS_TOKEN_KEY_PREFIX + accessToken;
        String refreshTokenKey = REFRESH_TOKEN_KEY_PREFIX + refreshToken + "_" + accessToken;
        redisOperator.set(accessTokenKey, refreshToken,saOauth2AppInfo.getAccessTokenTimeout());
        redisOperator.set(refreshTokenKey,refreshToken,saOauth2AppInfo.getRefreshTokenTimeout());

        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put("access_token", accessToken);
        map.put("refresh_token", refreshToken);
        map.put("expires_time", expiresTime);
        map.put("refresh_expires_time", refreshExpiresTime);
        return map;
    }

    /**
     * 检查access_token 是否有效
     * @param accessToken
     */
    public void checkTokenValid(String accessToken){
        String accessTokenKey = ACCESS_TOKEN_KEY_PREFIX + accessToken;
        String accessTokenRedis = redisOperator.get(accessTokenKey);
        if (StringUtils.isEmpty(accessTokenRedis)){
            throw new TokenException(TokenException.ACCESS_TOKEN_TIMEOUT);
        }
    }

    /**
     * 检查refresh_token是否有效
     * @param accessToken
     * @param reFreshToken
     */
    public void checkRefreshTokenValid(String accessToken,String reFreshToken){
        String refreshTokenKey = REFRESH_TOKEN_KEY_PREFIX + reFreshToken +"_"+ accessToken;
        String refreshTokenRedis = redisOperator.get(refreshTokenKey);
        if (StringUtils.isEmpty(refreshTokenRedis)){
            throw new TokenException(TokenException.REFRESH_TOKEN_TIMEOUT);
        }
        redisOperator.del(refreshTokenKey);
    }

    /**
     * 全局拦截调用校验 access_token 的函数
     */
    public  void checkAccessToken(){
        String tokenCute = getAccessTokenCute();
        System.out.println(tokenCute);
        checkTokenValid(tokenCute);
    }


    /**
     * 从请求头中获取token
     * @return
     */
    public String getAccessTokenCute(){
        SaRequest request = SaHolder.getRequest();
        SaTokenConfig config = SaManager.getConfig();
        String authorization = request.getHeader("Authorization");
        // 2. 如果打开了前缀模式，则裁剪掉
        String tokenPrefix = config.getTokenPrefix();
        if(SaFoxUtil.isEmpty(tokenPrefix) != false) {
            // 如果token并没有按照指定的前缀开头，则视为未提供token
            if (SaFoxUtil.isEmpty(authorization)){
                return  null;
            }
            if(authorization.startsWith(tokenPrefix + SaTokenConsts.TOKEN_CONNECTOR_CHAT) == false) {
                return authorization;
            } else {
                // 则裁剪掉前缀
                return authorization.substring(tokenPrefix.length() + SaTokenConsts.TOKEN_CONNECTOR_CHAT.length());
            }
        }
        return authorization;
    }

}

package com.neusoft.satoken.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Data
@Configuration
@ConfigurationProperties(prefix = "sa-oauth2-app-info")
public class SaOauth2AppInfo  {

    /**
     * token 长度，默认16个字符
     */
    private Integer tokenLength = 16;


    /**
     * access_token 有效期（单位s）,默认 1分钟
     */
    private Integer accessTokenTimeout = 60;


    /**
     * refresh_token 有效期（单位s）,默认1个小时
     */
    private Integer refreshTokenTimeout = 3600;

}
